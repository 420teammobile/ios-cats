//
//  HomeCoordinator.swift
//  cats
//
//  Created by Sebastian Panesso on 8/31/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import UIKit

protocol HomeFlow: class {
    func showBreedDetail(breed:Breed)
    func showLikesImages()
}

class HomeCoordinator: Coordinator, HomeFlow  {
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let homeViewController = Controllers.home()
        homeViewController.coordinator = self
        navigationController.pushViewController(homeViewController, animated: true)
    }
    
    func showBreedDetail( breed: Breed) {
        let tabBarCoordinator = BreedCoordinator(navigationController: navigationController,breed:breed)
        coordinate(to: tabBarCoordinator)
    }
    
    func showLikesImages( ) {
        let likesCoordinator = LikesCoordinator(navigationController: navigationController)
        coordinate(to: likesCoordinator)
    }
}
