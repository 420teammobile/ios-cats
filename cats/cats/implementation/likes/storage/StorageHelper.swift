//
//  StorageHelper.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/29/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import RealmSwift
import PromiseKit

class StorageHelper{
    
    let uiRealm = try! Realm()
    
    private init() {}
    
    public static let shared = StorageHelper()
    
    public func loadLikesImages() -> [LikeImage]{
        return Array(uiRealm.objects(LikeImage.self))
    }
    
    public func addNewLikeImage(like: LikeImage) {
        try! uiRealm.write {
            uiRealm.add(like)
        }
    }
    
    public func clearAllLikeImages() {
        try! uiRealm.write {
            uiRealm.delete(uiRealm.objects(LikeImage.self))
        }
    }
    
    
    public func toggleLike(like:LikeImage) -> Promise<[LikeImage]> {
        return Promise { seal in
            do {
                try! uiRealm.write {
                    uiRealm.add(like, update: Realm.UpdatePolicy.all)
                    seal.fulfill(Array(uiRealm.objects(LikeImage.self)))
                }
            }catch{
                seal.reject(RequestError.NoData)
            }
        }
    }
  
}
