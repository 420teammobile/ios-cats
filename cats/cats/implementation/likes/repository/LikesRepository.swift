//
//  LikesRepository.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/29/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class LikesRepository:BaseRepository {
    
    override
    private init(){}
    
    public static let shared = LikesRepository()

    public func getCatImages() -> Promise<[LikeImage]> {
        return Promise { seal in
            let url = Constants.request.getCatsImages
            var images: [LikeImage] = []
            self.requestGET(url: url).done{
                (response) in
                if let data = response as? Data{
                    images = self.parser.decodeLikeImage(from:data)
                    seal.fulfill(images)
                } else{
                    seal.reject(RequestError.NoData)
                }
            }
        }
    }
}
