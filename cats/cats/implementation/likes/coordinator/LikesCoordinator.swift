//
//  LikesCoordinator.swift
//  cats
//
//  Created by Sebastian Panesso on 9/1/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import UIKit

class LikesCoordinator: Coordinator {
    
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController ) {
        self.navigationController = navigationController
    }

    func start() {
        let likesViewController = Controllers.likesImages()
        likesViewController.coordinator = self
        navigationController.pushViewController(likesViewController, animated: true)
    }
}

