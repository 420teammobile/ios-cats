//
//  BreedRepository.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/28/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit


class BreedRepository:BaseRepository {
    
    override
    private init(){}
    
    public static let shared = BreedRepository()
    
    public func getBreeds() -> Promise<[Breed]> {
        return Promise { seal in
            let url = Constants.request.getBreeds
            var breeds: [Breed] = []
            self.requestGET(url: url).done{
                (response) in
                if let data = response as? Data{
                    breeds = self.parser.decode(from:data)
                    seal.fulfill(breeds)
                } else{
                    seal.reject(RequestError.NoData)
                }
            }
        }
    }
    
    public func getBreedImageUrl(url:String) -> Promise<String> {
        return Promise { seal in
            self.requestGET(url: url).done{
                (response) in
                if let data = response as? Data{
                    let url = self.parser.getBreedImage(from:data)
                    seal.fulfill(url)
                } else{
                    seal.reject(RequestError.NoData)
                }
            }
        }
    }
}
