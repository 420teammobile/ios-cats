//
//  BreedDataManager.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/28/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//


import Foundation
import PromiseKit

class BreedDataManager {
    
    private let repository = BreedRepository.shared
    
    private init() {}
    
    public static let shared = BreedDataManager()
    
    public func loadBreeds() -> Promise<[Breed]> {
        
        return Promise { seal in
            firstly(execute: self.repository.getBreeds)
                .done {
                    (breeds)in
                    seal.fulfill(breeds)
            }
        }
    }
    
    public func getBreedImage(url:String) -> Promise<String>{
        return Promise { seal in
            self.repository.getBreedImageUrl(url:url).done{
                (url)in
                if !url.isEmpty{
                    seal.fulfill(url)
                }else{
                    seal.reject(RequestError.NoData)
                }
            }
        }
    }
}
