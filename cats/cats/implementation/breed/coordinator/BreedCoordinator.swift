//
//  BreedCoordinator.swift
//  cats
//
//  Created by Sebastian Panesso on 9/1/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//


import Foundation
import UIKit

class BreedCoordinator: Coordinator {
    
    let navigationController: UINavigationController
    let breed:Breed
    
    init(navigationController: UINavigationController,breed:Breed) {
        self.navigationController = navigationController
        self.breed = breed
    }

    func start() {
        let breedDetailViewController = Controllers.breedInfo()
        breedDetailViewController.coordinator = self
        breedDetailViewController.breed = self.breed
        
        navigationController.pushViewController(breedDetailViewController, animated: true)
    }
}
