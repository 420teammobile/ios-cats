//
//  BaseRepository.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/28/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

enum RequestError: Error {
    case NoData
}

class BaseRepository {
    
    var parser:JSONParser!
    
    init(){
        self.parser = JSONParser()
    }
    public func requestGET(url:String) -> Promise<Data> {
        return Promise { seal in
            AF.request(url, method: .get,encoding: URLEncoding.default, headers: nil)
                .responseJSON(completionHandler: {
                    response in
                    guard response.response?.statusCode == 200 else {
                        seal.reject(RequestError.NoData)
                        return
                    }
                    seal.fulfill(response.data!)
                })
        }
    }
    
    
}
