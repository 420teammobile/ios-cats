//
//  BaseViewController.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/28/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = Constants.app.name
              navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:Constants.colors.orange]
    }
    
}
