//
//  UIImageView+Download.swift
//  thecats
//
//  Created by Sebastian Panesso on 8/29/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage


extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        AF.request(url).responseImage { response in
            if case .success(let image) = response.result {
                DispatchQueue.main.async() { [weak self] in
                    self?.image = image
                }
            }
        }
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
