//
//  Coordinator.swift
//  cats
//
//  Created by Sebastian Panesso on 8/31/20.
//  Copyright © 2020 Sunbelt. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
    func coordinate(to coordinator: Coordinator)
}

extension Coordinator {
    func coordinate(to coordinator: Coordinator) {
        coordinator.start()
    }
}
