# SUNBELT iOS TEST #

Prueba en iOS para el proceso de selección SUNBELT

### API Pública utilizada ###

* TheCatApi // Developer Experience
* https://docs.thecatapi.com/

### Tecnologías implementadas ###

* Alamofire
* Arquitectura MVVM
* Coordinators
* RxSwift/RxCocoa
* Dependency injections
* Image loading (Alamofire Image, SDWebImage)
* PromiseKit
* Realm

### Implementación ###

* Listado de razas de gatos
* Detalle de la raza seleccionada
* Lista de imagenes para dar Like, almacenado en base de datos

### Manejo de repositorio ###

* Ramas: master, qa, dev
* Solicitud de pull request

### Desarrollador ###

* Sebastian Alfredo Panesso Laverde
* spanesso@gmail.com